(function(){
	angular
		.module('ServiceApp')
		.factory('ServiceFactory',ServiceFactory);
		function ServiceFactory($q,$http){
			var fac = {};
			fac.getService=function(){
				var defer=$q.defer();
				$http.get('api/services')
					.success(function(data){
						defer.resolve(data);
					})
					.error(function(err){
						defer.reject(err);
					});
				return defer.promise;
			}
			return fac;
		}
})();