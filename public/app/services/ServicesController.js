(function(){
	angular
		.module('ServiceApp')
		.controller('ServicesController',ServicesController);
		function ServicesController(ServiceFactory,$state){
			var vm = this;
        	vm.services = {};
        	ServiceFactory.getService()
            	.then(function(data){
                	vm.services = data;
            	})
            	.catch(function(err){
                console.log(err);
            });
		}
})();