(function(){
	angular
		.module('ServiceApp',['ui.router'])
		.config(function($stateProvider,$urlRouterProvider){
			$urlRouterProvider
				.otherwise('/');
			$stateProvider
				.state('services',{
					url : '/',
					templateUrl: 'app/services/Service.html',
					controller : 'ServicesController',
					controllerAs : 'ctrl'
				});
		});
})();